#!/bin/sh

enable_domains_rules() {

   for i in `cat "$(dirname "$0")/wgsitelist.txt"`; do
        for j in `nslookup $i | egrep -v 127.0.0.1 | awk '/Addr/ {print $3}' | egrep -v ":"`; do
                EXISTS=`ip route show $j | wc -l`
                if [ $EXISTS -eq 0 ]
                then
                        route add -host $j dev $IFACE
                        logger "Add $j host to static route for vpn $i"
                fi
                if [ $EXISTS -eq 1 ]
                then
                        logger "Host $j is already in static route to vpn $i"
                fi
        done
    done

    ip route flush cache 
}

disable_wg_routes() {

   for i in `route | egrep $IFACE | grep -v $(echo $ADDR | sed 's/.$//') | awk '{print $1}'`; do

         route del -host $i dev $IFACE 
         logger "Del $i ip to static routes wg"
   done

   ip route flush cache
}


domains_rules() {

. "$(dirname "$0")/conf.sh"

case "$1" in
    enable)
      enable_domains_rules
      echo -en "Setting up WireGuard domains iptables rules... \n"
      ;;

    disable)
      disable_wg_routes
      echo -en "Removing WireGuard domains iptables rules... \n"
      ;;

    *)
      echo "Wrong argument: 'enable' or 'disable' expected. Doing nothing." >&2
      return
      ;;
esac

}


case "$1" in
  enable)
    domains_rules disable 2> /dev/null
    domains_rules enable
    ;;

  disable)
    domains_rules disable
    ;;

  *)
    echo "Usage: $0 {enable|disable}" >&2
    exit 1
    ;;
esac

exit 0 