#!/bin/sh

set +x

### Custom user script to start wireguard
### First param is:
###  "start" (call at start optware),
###  "stop" (call before stop optware),

IFACE=wg1
ADDR=10.11.0.1
MASK=24
PORT=53663
CFG="$(dirname "$0")/wg_srv.conf"
WAN="$(nvram get wan_ifname)"
WGGO=/opt/bin/wireguard-go

log() {
	logger $1
	echo -e $1
}

case "$1" in
start)
	#
        EXISTS=`wg show wg1 2> /dev/null | grep "transfer:" | wc -l`
        if [ $EXISTS -eq 0 ]
        then

  		[ -e /sys/module/wireguard ] `modprobe wireguard`

		[ ! -f "$WGGO" ] && \
                log "wireguard-go not found, plz install using opkg ... " && \
                exit 1

		($WGGO $IFACE 2> /dev/null) && \
		(ip address add dev $IFACE $ADDR/$MASK) && \
		(wg setconf $IFACE $CFG) && \
                (sleep 1) && \
                (ip link set up dev $IFACE) 
		
		#add rules		
        	iptables -t filter -A INPUT -i $IFACE -j ACCEPT
        	iptables -t filter -A FORWARD -i $IFACE -j ACCEPT
        	iptables -I INPUT 1 -i $WAN -p udp --dport $PORT -j ACCEPT
        	iptables -A FORWARD -i $IFACE -j ACCEPT; iptables -t nat -A POSTROUTING -o $WAN -j MASQUERADE

        	log "Wireguard server started... "
        fi
        if [ $EXISTS -eq 1 ]
        then
             log "Wireguard server already started..."
        fi
	;;
stop)
	#
	iptables -t filter -D INPUT -i $IFACE -j ACCEPT
	iptables -t filter -D FORWARD -i $IFACE -j ACCEPT
        iptables -D FORWARD -i $IFACE -j ACCEPT; iptables -t nat -D POSTROUTING -o $WAN -j MASQUERADE
	ip link set down dev $IFACE
	ip link delete dev $IFACE
	log "Wireguard server stopped... "
	;;
check|status)
	wg showconf $IFACE
	wg show
	;;
*)
	echo "Usage: $0 {start|stop|check|status}"
	exit 1
	;;
esac

